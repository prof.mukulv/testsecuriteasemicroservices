/**
 * 
 */
package com.org.framework.automation.tools.testrail;

import org.apache.commons.lang.StringUtils;

import com.org.framework.tools.utils.PropFileHandler;

/**
 * @author Mukul Varshney
 *
 */
public class ConfigReader {

	
	public String getTestRailsUserName() {
		String testRailsUserName = System.getProperty("testRailsUserName");
		if (StringUtils.isBlank(testRailsUserName)) {
			testRailsUserName = PropFileHandler.readProperty("username", "testRailsConfig");
			System.out.println(testRailsUserName);
			if (StringUtils.isBlank(testRailsUserName)) {
				System.err.println("Could not find TestRails username!!");
			}
		}
		return testRailsUserName;
	}
	public String getTestRailsPassword() {
		String testRailsPassword = System.getProperty("testRailsPassword");
		if (StringUtils.isBlank(testRailsPassword)) {
			testRailsPassword = PropFileHandler.readProperty("password", "testRailsConfig");
			System.out.println(testRailsPassword);
			if (StringUtils.isBlank(testRailsPassword)) {
				System.err.println("Could not find TestRails password!!");
			}
		}
		return testRailsPassword;
	}
	public String getTestRailsEngineURL() {
		String testRailsEngineURL = System.getProperty("testRailsEngineURL");
		if (StringUtils.isBlank(testRailsEngineURL)) {
			testRailsEngineURL = PropFileHandler.readProperty("testRailsEngineURL", "testRailsConfig");
			if (StringUtils.isBlank(testRailsEngineURL)) {
				System.err.println("Could not find TestRails engine URL!!");
			}
		}
		return testRailsEngineURL;
	}
	public String getTestRailsTestRunId() {
		String testRailsTestRunId = System.getProperty("testRailsTestRunId");
		if (StringUtils.isBlank(testRailsTestRunId)) {
			testRailsTestRunId = PropFileHandler.readProperty("testRunId", "testRailsConfig");
			if (StringUtils.isBlank(testRailsTestRunId)) {
				System.err.println("Could not find TestRails Test Run Id!!");
			}
		}
		return testRailsTestRunId;
	}
}
