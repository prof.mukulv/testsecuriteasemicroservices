/**
 * 
 */
package com.org.framework.tools.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.junit.Assert;

/**
 * @author mukul
 *
 */
public class CommandExecutor {
	public static void launchConsoleAndExecuteScript(String shellScriptFileName) {
		String shellScriptFilePath = System.getProperty("user.dir") + File.separatorChar + "src" + File.separatorChar
				+ "test" + File.separatorChar + "resources" + File.separatorChar + "shell_scripts" + File.separatorChar
				+ shellScriptFileName;
		ProcessBuilder processBuilder = new ProcessBuilder(shellScriptFilePath);

		try {

			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.contains("Tests:") && line.contains("succeeded") && line.contains("succeeded")
						&& line.contains("failed") && line.contains("canceled")) {
					String[] newData = line.split(": ");
					String[] results = newData[1].split(", ");
					String succeededTests = results[0].split(" ")[1];
					Assert.assertEquals("3", succeededTests);
				}
			}

			int exitcode = process.waitFor();

			System.out.println("Exit code: " + exitcode);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	public static void launchConsoleAndExecuteScript2(String shellScriptFileName) {
		String shellScriptFilePath = System.getProperty("user.dir") + File.separatorChar + "src" + File.separatorChar
				+ "test" + File.separatorChar + "resources" + File.separatorChar + "shell_scripts" + File.separatorChar
				+ shellScriptFileName;
		ProcessBuilder processBuilder = new ProcessBuilder(shellScriptFilePath);

		try {

			Process process = processBuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.contains("Tests:") && line.contains("succeeded") && line.contains("succeeded")
						&& line.contains("failed") && line.contains("canceled")) {
					String[] newData = line.split(": ");
					String[] results = newData[1].split(", ");
					String succeededTests = results[0].split(" ")[1];
					Assert.assertEquals("2", succeededTests);
				}
			}

			int exitcode = process.waitFor();

			System.out.println("Exit code: " + exitcode);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}
}
