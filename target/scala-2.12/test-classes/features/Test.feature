Feature: To run smoke and load test

@abc
Scenario: To run smoke test
Given user checked out to the target directory of this project
When clone ingestion-service project in a target directory
And navigate to root directory of ingestion service project
And run sbt test command to run both Smoke and load test
Then verify results of smoke tests and update results on testrail