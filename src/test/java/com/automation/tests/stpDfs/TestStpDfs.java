/**
 * 
 */
package com.automation.tests.stpDfs;


import com.org.framework.tools.utils.CommandExecutor;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author mukul
 *
 */
public class TestStpDfs {

	@Given("^user checked out to the target directory of this project$")
	public void user_checked_out_to_the_target_directory_of_this_project() throws Throwable {
		
	}

	@When("^clone ingestion\\-service project in a target directory$")
	public void clone_ingestionservice_project_in_a_target_directory() throws Throwable {
		
	}

	@Then("^verify results of smoke tests and update results on testrail$")
	public void verify_results_of_smoke_tests_and_update_results_on_testrail() throws Throwable {

	}

	@When("^navigate to root directory of ingestion service project$")
	public void navigate_to_root_directory_of_ingestion_service_project() throws Throwable {
	}

	@When("^run sbt test command to run both Smoke and load test$")
	public void run_sbt_test_command_to_run_both_smoke_and_load_test() throws Throwable {
	}
	
	@When("^execute shell script for running smoke tests and parse test results$")
    public void execute_shell_script_for_running_smoke_tests_and_parse_test_results() throws Throwable {
		CommandExecutor.launchConsoleAndExecuteScript("run_smoke_tests_by_local_repo.sh");
	}

	@When("^execute shell script for running smoke tests and parse test results for failing test$")
    public void execute_shell_script_for_running_smoke_tests_and_parse_test_results_for_failing_test() throws Throwable {
		CommandExecutor.launchConsoleAndExecuteScript2("run_smoke_tests_by_local_repo.sh");
	}
}
