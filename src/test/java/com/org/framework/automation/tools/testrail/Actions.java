package com.org.framework.automation.tools.testrail;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.org.framework.tools.utils.APIClient;

public class Actions {
	public static String testCaseIDs[] = { "ID_1", "ID_2", "ID_3" };
	public static ConfigReader configReader = new ConfigReader();
	public static String TEST_RUN_ID = configReader.getTestRailsTestRunId();
	public static String TESTRAIL_USERNAME = configReader.getTestRailsUserName();
	public static String TESTRAIL_PASSWORD = configReader.getTestRailsPassword();
	public static String RAILS_ENGINE_URL = configReader.getTestRailsEngineURL();

	public static final int TEST_CASE_PASSED_STATUS = 1;
	public static final int TEST_CASE_FAILED_STATUS = 5;

	public static void addResultForTestCase(String testCaseId, int status, String error) throws Exception {

		String testRunId = TEST_RUN_ID;

		APIClient client = new APIClient(RAILS_ENGINE_URL);
		client.setUser(TESTRAIL_USERNAME);
		client.setPassword(TESTRAIL_PASSWORD);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("status_id", status);
		data.put("comment", error);

		JSONObject r = (JSONObject) client.sendPost("add_result_for_case/" + testRunId + "/" + testCaseId + "", data);
		System.out.println(r);

	}

	public static APIClient myClient() {
		APIClient client;
		client = new APIClient("https://mytestraillink.net/"); // i hide it
		client.setUser("userName");// i hide it
		client.setPassword("password");// i hide it
		return client;
	}
}
