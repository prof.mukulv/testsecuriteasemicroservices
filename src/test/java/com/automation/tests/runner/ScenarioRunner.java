/**
 * 
 */
package com.automation.tests.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * @author mukul
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "json:target/cucumber.json",
		"html:target/cucumber-report.html" }, features = "src/test/resources/features", glue = {

				"com.automation.tests.stpDfs" }, tags = "@abc")
public class ScenarioRunner {
	
}
