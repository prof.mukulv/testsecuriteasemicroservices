/**
 * 
 */
package com.automation.tests.stpDfs;

/**
 * @author mukul
 *
 */
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;

import org.apache.commons.lang3.reflect.FieldUtils;

import com.org.framework.automation.tools.testrail.Actions;
import com.org.framework.tools.utils.PropFileHandler;

import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
public class BaseInit {
	public static Scenario scenario;
	public static String featureName;

	@Before
	public void setUp(Scenario scenario) throws Exception {

		System.out.println("STARTING TEST..... ");
		String[] scenarioTags = scenario.getSourceTagNames().toArray(new String[] {});
		String TESTRAIL_ID = null;
		for (String tag : scenarioTags) {
			if (tag.contains("TESTRAIL_ID=")) {
				tag.replace("@", "");
				TESTRAIL_ID = tag.substring(13);
			}
		}
		System.out.println("Test rail id of scenario: \t" + TESTRAIL_ID);
		PropFileHandler.writeToFile("TestCaseID", TESTRAIL_ID);
		Thread.sleep(5000);



	}

	@After
	public void tearDown(Scenario scenario) throws Exception {
		String output = scenario.getId().split(";")[0];

		this.scenario = scenario;
		String ID = PropFileHandler.readProperty("TestCaseID");
		if (scenario.isFailed()) {

			// logError(scenario, ID);

			// Take a screenshot...

			System.out.println("The error message is " + scenario.getName());
			System.out.println("The  message is " + scenario.getStatus());
			Actions.addResultForTestCase(ID, 5, "Test has been failed");
		} else {
			Actions.addResultForTestCase(ID, 1, "Test has been successfully passed");
			System.out.println("Updated Pass testcase with is:-" + ID);
		}
	}

	private void logError(Scenario scenario, String ID) {

		Field field = FieldUtils.getField(((Scenario) scenario).getClass(), "stepResults", true);
		field.setAccessible(true);
		try {
			ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
			for (Result result : results) {
				if (result.getError() != null)
					Actions.addResultForTestCase(ID, 5, "Error:-" + result.getErrorMessage());
				System.out.println("Updated Fail testcase with is:-" + ID);

			}
		} catch (Exception e) {

			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			System.out.println(exceptionAsString);
			e.printStackTrace();

			System.out.println("*********************" + exceptionAsString);

		}
	}
}
