package com.org.framework.tools.utils;

/**
 * @author Mukul Varshney
 *
 */
import java.util.UUID;

public class UUIDGenerator {
	/*
	 * This menthod generates 36 digits v4 UUID
	 * Eg. e3aed661-ccc2-42fd-ad69-734fee350cd2
	 * */
	public static String generate36DigitsV4UUID() {
		// creating random uuid i.e. version 4 UUID
		UUID uuid = UUID.randomUUID();
		System.out.println("Printing the randomly generated UUID value......\n");
		System.out.println("uuid: " + uuid);
		return uuid.toString();
	}


}
