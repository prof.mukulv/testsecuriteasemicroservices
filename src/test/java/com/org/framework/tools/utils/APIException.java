package com.org.framework.tools.utils;

public class APIException extends Exception
{
	public APIException(String message)
	{
		super(message);
	}
}
