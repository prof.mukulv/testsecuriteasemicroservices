package com.org.framework.tools.utils;


import org.testng.Reporter;

import com.org.framework.tools.utils.ConfigFileReader;

/**
 * @author nimit.jain
 *
 */
public class Logger {
	
	public static void log(String msg){
		Reporter.log(msg, ConfigFileReader.getLogToConsole());
	}
	
	public static void log(String msg, int level) {
		Reporter.log(msg, level, ConfigFileReader.getLogToConsole());
	}
}
